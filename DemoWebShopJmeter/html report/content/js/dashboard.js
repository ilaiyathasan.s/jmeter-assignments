/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.746268656716418, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.25, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.25, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 66, 0, 0.0, 1287.5151515151517, 172, 18916, 303.0, 1648.3, 8584.599999999997, 18916.0, 1.7482980583295806, 60.975930273370246, 3.1708597123255013], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/logout-23", 1, 0, 0.0, 408.0, 408, 408, 408.0, 408.0, 408.0, 408.0, 2.450980392156863, 0.2967984068627451, 2.197265625], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 1, 0, 0.0, 408.0, 408, 408, 408.0, 408.0, 408.0, 408.0, 2.450980392156863, 0.2967984068627451, 2.173330269607843], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 1, 0, 0.0, 306.0, 306, 306, 306.0, 306.0, 306.0, 306.0, 3.2679738562091503, 0.3925398284313726, 2.913730596405229], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 1, 0, 0.0, 306.0, 306, 306, 306.0, 306.0, 306.0, 306.0, 3.2679738562091503, 0.39892258986928103, 2.8722426470588234], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 1, 0, 0.0, 7308.0, 7308, 7308, 7308.0, 7308.0, 7308.0, 7308.0, 0.13683634373289547, 58.89668749144773, 0.15207007731253422], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 1, 0, 0.0, 263.0, 263, 263, 263.0, 263.0, 263.0, 263.0, 3.802281368821293, 11.38456511406844, 4.3221245247148286], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 2, 0, 0.0, 14094.0, 9272, 18916, 14094.0, 18916.0, 18916.0, 18916.0, 0.06530825496342739, 36.342895890478054, 1.242515061716301], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 1, 0, 0.0, 3775.0, 3775, 3775, 3775.0, 3775.0, 3775.0, 3775.0, 0.26490066225165565, 10.062603476821192, 5.775041390728477], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 1, 0, 0.0, 952.0, 952, 952, 952.0, 952.0, 952.0, 952.0, 1.050420168067227, 4.0416557247899165, 1.2073677127100841], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 1, 0, 0.0, 281.0, 281, 281, 281.0, 281.0, 281.0, 281.0, 3.558718861209964, 13.696202179715302, 4.027885898576512], "isController": false}, {"data": ["Test", 1, 0, 0.0, 31963.0, 31963, 31963, 31963.0, 31963.0, 31963.0, 31963.0, 0.03128617463942684, 36.00885936473422, 1.8725264368175703], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 1, 0, 0.0, 219.0, 219, 219, 219.0, 219.0, 219.0, 219.0, 4.5662100456621, 17.573665810502284, 5.230629280821918], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 1, 0, 0.0, 219.0, 219, 219, 219.0, 219.0, 219.0, 219.0, 4.5662100456621, 17.636094463470318, 5.203874143835616], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 1, 0, 0.0, 201.0, 201, 201, 201.0, 201.0, 201.0, 201.0, 4.975124378109452, 0.607314987562189, 4.377526430348259], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 1, 0, 0.0, 1057.0, 1057, 1057, 1057.0, 1057.0, 1057.0, 1057.0, 0.9460737937559129, 7.036423841059603, 1.0698764191106906], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 2, 0, 0.0, 722.0, 199, 1245, 722.0, 1245.0, 1245.0, 1245.0, 0.33400133600534404, 4.184801895457581, 0.32405305402471607], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 2, 0, 0.0, 1198.5, 748, 1649, 1198.5, 1649.0, 1649.0, 1649.0, 0.3459609064175748, 21.806686235080438, 0.314202776336274], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 2, 0, 0.0, 923.5, 199, 1648, 923.5, 1648.0, 1648.0, 1648.0, 0.33416875522138684, 15.432624530075188, 0.326499843358396], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 2, 0, 0.0, 631.5, 194, 1069, 631.5, 1069.0, 1069.0, 1069.0, 0.33444816053511706, 4.4781432901337785, 0.32922240802675584], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 2, 0, 0.0, 530.5, 194, 867, 530.5, 867.0, 867.0, 867.0, 0.33428046130703665, 1.3684606384756812, 0.31599949857930804], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 2, 0, 0.0, 636.0, 200, 1072, 636.0, 1072.0, 1072.0, 1072.0, 0.3339455668725998, 3.9996830126064453, 0.3119325534312907], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 2, 0, 0.0, 253.5, 201, 306, 253.5, 306.0, 306.0, 306.0, 0.3834355828220859, 1.4264627468366564, 0.3692065279907975], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 2, 0, 0.0, 610.0, 199, 1021, 610.0, 1021.0, 1021.0, 1021.0, 0.390777647518562, 43.61215929073857, 0.3762761332551778], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 2, 0, 0.0, 8840.5, 704, 16977, 8840.5, 16977.0, 16977.0, 16977.0, 0.09067827348567283, 1.0625474644087776, 0.05999465848295248], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 1, 0, 0.0, 178.0, 178, 178, 178.0, 178.0, 178.0, 178.0, 5.617977528089887, 0.6803019662921349, 4.97607970505618], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 1, 0, 0.0, 176.0, 176, 176, 176.0, 176.0, 176.0, 176.0, 5.681818181818182, 0.6824840198863636, 5.060369318181818], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 1, 0, 0.0, 213.0, 213, 213, 213.0, 213.0, 213.0, 213.0, 4.694835680751174, 0.5685152582159625, 4.071302816901409], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 1, 0, 0.0, 213.0, 213, 213, 213.0, 213.0, 213.0, 213.0, 4.694835680751174, 0.5685152582159625, 4.052963615023474], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 1, 0, 0.0, 1497.0, 1497, 1497, 1497.0, 1497.0, 1497.0, 1497.0, 0.6680026720106881, 23.01869363727455, 0.4964355794923179], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 1, 0, 0.0, 807.0, 807, 807, 807.0, 807.0, 807.0, 807.0, 1.2391573729863692, 0.8361892038413878, 1.2851417286245352], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 1, 0, 0.0, 201.0, 201, 201, 201.0, 201.0, 201.0, 201.0, 4.975124378109452, 0.597597947761194, 4.309507151741293], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 1, 0, 0.0, 203.0, 203, 203, 203.0, 203.0, 203.0, 203.0, 4.926108374384237, 0.5965209359605911, 4.214131773399014], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 1, 0, 0.0, 185.0, 185, 185, 185.0, 185.0, 185.0, 185.0, 5.405405405405405, 0.6492820945945946, 4.608319256756757], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 1, 0, 0.0, 189.0, 189, 189, 189.0, 189.0, 189.0, 189.0, 5.291005291005291, 0.640707671957672, 4.743303571428571], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 2, 0, 0.0, 269.5, 239, 300, 269.5, 300.0, 300.0, 300.0, 0.39619651347068147, 0.6844449534469097, 0.38149390847860537], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 1, 0, 0.0, 735.0, 735, 735, 735.0, 735.0, 735.0, 735.0, 1.3605442176870748, 0.16475340136054423, 1.1559311224489797], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 1, 0, 0.0, 190.0, 190, 190, 190.0, 190.0, 190.0, 190.0, 5.263157894736842, 0.6373355263157895, 4.528166118421052], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 1, 0, 0.0, 195.0, 195, 195, 195.0, 195.0, 195.0, 195.0, 5.128205128205129, 0.6209935897435898, 4.427083333333333], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 1, 0, 0.0, 263.0, 263, 263, 263.0, 263.0, 263.0, 263.0, 3.802281368821293, 0.4530061787072243, 4.570906606463878], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 1, 0, 0.0, 377.0, 377, 377, 377.0, 377.0, 377.0, 377.0, 2.6525198938992043, 144.41468418435014, 2.9478199602122017], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 1, 0, 0.0, 306.0, 306, 306, 306.0, 306.0, 306.0, 306.0, 3.2679738562091503, 0.3957312091503268, 2.888199550653595], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 1, 0, 0.0, 375.0, 375, 375, 375.0, 375.0, 375.0, 375.0, 2.6666666666666665, 77.66927083333333, 3.0234375], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 1, 0, 0.0, 509.0, 509, 509, 509.0, 509.0, 509.0, 509.0, 1.9646365422396854, 0.3798808939096267, 1.68835952848723], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 1, 0, 0.0, 263.0, 263, 263, 263.0, 263.0, 263.0, 263.0, 3.802281368821293, 0.45671934410646386, 4.455798479087452], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 1, 0, 0.0, 567.0, 567, 567, 567.0, 567.0, 567.0, 567.0, 1.763668430335097, 0.21356922398589068, 1.513930224867725], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 2, 0, 0.0, 236.0, 172, 300, 236.0, 300.0, 300.0, 300.0, 0.4160599126274183, 1.541940789473684, 0.40102649781568545], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 1, 0, 0.0, 567.0, 567, 567, 567.0, 567.0, 567.0, 567.0, 1.763668430335097, 0.21012455908289243, 1.527708884479718], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 1, 0, 0.0, 298.0, 298, 298, 298.0, 298.0, 298.0, 298.0, 3.3557046979865772, 0.4063548657718121, 3.9848993288590604], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 1, 0, 0.0, 568.0, 568, 568, 568.0, 568.0, 568.0, 568.0, 1.7605633802816902, 0.2114739216549296, 1.4717209507042255], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 2, 0, 0.0, 252.5, 205, 300, 252.5, 300.0, 300.0, 300.0, 0.39888312724371755, 0.7779000049860391, 0.3838860565416833], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 1, 0, 0.0, 567.0, 567, 567, 567.0, 567.0, 567.0, 567.0, 1.763668430335097, 0.21356922398589068, 1.5535438712522047], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 2, 0, 0.0, 250.5, 201, 300, 250.5, 300.0, 300.0, 300.0, 0.3992015968063872, 0.3032996506986028, 0.3806839446107785], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 1, 0, 0.0, 734.0, 734, 734, 734.0, 734.0, 734.0, 734.0, 1.3623978201634876, 0.16497786103542234, 1.16016689373297], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 66, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
