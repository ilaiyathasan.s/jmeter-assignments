/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 83.33333333333333, "KoPercent": 16.666666666666668};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5988372093023255, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.4, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc"], "isController": false}, {"data": [0.9, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/-5"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/-3"], "isController": false}, {"data": [0.35, 500, 1500, "https://opensource-demo.orangehrmlive.com/-2"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/-1"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/-0"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts"], "isController": false}, {"data": [0.95, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2"], "isController": false}, {"data": [0.95, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0"], "isController": false}, {"data": [0.95, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6"], "isController": false}, {"data": [0.95, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-23"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-23&currentTime=11:51"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 420, 70, 16.666666666666668, 1837.3285714285698, 239, 29945, 403.5, 5094.900000000003, 12941.449999999963, 24058.65000000001, 6.18474723527073, 1264.3220154913192, 5.713643441959092], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout", 10, 0, 0.0, 1238.5, 1050, 1716, 1160.0, 1704.6000000000001, 1716.0, 1716.0, 0.30498963035256804, 3.257670489203367, 1.3560720576125413], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0", 10, 0, 0.0, 394.00000000000006, 368, 413, 395.0, 413.0, 413.0, 413.0, 0.5098659052669148, 0.8260425164431755, 0.40231606587467494], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1", 10, 0, 0.0, 425.4, 396, 475, 424.0, 471.20000000000005, 475.0, 475.0, 0.5091390458734281, 2.2107274801435772, 0.31174822437757754], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2", 10, 0, 0.0, 262.79999999999995, 240, 327, 261.0, 321.1, 327.0, 327.0, 0.5131889561736632, 0.4966506402032228, 0.3623394680796469], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 10, 10, 100.0, 417.40000000000003, 393, 486, 413.5, 479.70000000000005, 486.0, 486.0, 0.5149595756733096, 0.164947989082857, 0.26753759204902416], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate", 10, 0, 0.0, 1098.6000000000001, 1071, 1129, 1099.5, 1128.0, 1129.0, 1129.0, 0.4924895345973898, 5.324225483255356, 2.4412860132972174], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3", 10, 0, 0.0, 259.59999999999997, 239, 327, 252.5, 321.0, 327.0, 327.0, 0.5131889561736632, 0.4976529623832495, 0.368353401159807], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/", 10, 0, 0.0, 24138.600000000002, 22037, 29945, 23308.0, 29625.4, 29945.0, 29945.0, 0.3290556103981573, 1385.7463328294668, 1.2802319842053307], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4", 10, 0, 0.0, 266.0, 243, 327, 261.5, 322.3, 327.0, 327.0, 0.5131099594643133, 0.49757635717584275, 0.36328585997229207], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5", 10, 0, 0.0, 266.40000000000003, 240, 327, 265.0, 322.3, 327.0, 327.0, 0.5131626212346693, 0.49812856006568484, 0.3678333632678196], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6", 10, 0, 0.0, 270.2, 241, 327, 265.0, 322.70000000000005, 327.0, 327.0, 0.5137426149499101, 0.4986915617775495, 0.3632320832263036], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 10, 10, 100.0, 421.5, 389, 481, 409.5, 479.9, 481.0, 481.0, 0.5142974696564493, 0.16473590824933143, 0.25765097845093604], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages", 30, 0, 0.0, 459.76666666666677, 372, 850, 399.5, 677.9, 780.1499999999999, 850.0, 0.6643635397289397, 21.42096634445035, 0.3308841848259368], "isController": false}, {"data": ["Test", 10, 10, 100.0, 31885.4, 29501, 37657, 30910.5, 37325.4, 37657.0, 37657.0, 0.26214381209531545, 1138.3405088375232, 5.766395866647443], "isController": true}, {"data": ["https://opensource-demo.orangehrmlive.com/-6", 10, 0, 0.0, 6514.9, 5004, 13104, 5661.5, 12489.300000000003, 13104.0, 13104.0, 0.7577479730241721, 1114.30983367432, 0.433633117375161], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/-5", 10, 0, 0.0, 6334.900000000001, 5105, 9853, 5553.5, 9802.8, 9853.0, 9853.0, 1.0098969905069684, 1548.3722906357302, 0.5877916077560089], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/-4", 10, 0, 0.0, 4048.4, 3508, 5938, 3960.5, 5745.400000000001, 5938.0, 5938.0, 1.5708451146716933, 1022.5051175188502, 0.9020087181903865], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/-3", 10, 0, 0.0, 3675.2, 2232, 7481, 3512.5, 7249.700000000001, 7481.0, 7481.0, 1.2639029322548028, 694.681289101997, 0.7380995639534884], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/-2", 10, 0, 0.0, 1124.1, 773, 1542, 1025.5, 1539.6, 1542.0, 1542.0, 5.827505827505828, 9.526606206293707, 3.346263111888112], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/-1", 10, 0, 0.0, 1036.1999999999998, 806, 1422, 935.0, 1421.6, 1422.0, 1422.0, 7.032348804500703, 30.29197982594937, 3.6123197960618847], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/-0", 10, 0, 0.0, 15944.2, 15502, 16347, 15951.5, 16341.4, 16347.0, 16347.0, 0.6117330397014743, 0.6296549060989783, 0.2998925643849024], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 10, 10, 100.0, 414.6, 395, 482, 408.5, 476.40000000000003, 482.0, 482.0, 0.515331100231899, 0.16506699304303013, 0.2687371167224942], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push", 10, 0, 0.0, 1143.3, 1078, 1292, 1123.0, 1281.9, 1292.0, 1292.0, 0.4969684921975947, 5.308438835602823, 1.7442429306231986], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5", 10, 0, 0.0, 261.8, 240, 291, 263.0, 289.6, 291.0, 291.0, 0.518887505188875, 0.5036857228102948, 0.2716051784973018], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4", 10, 0, 0.0, 257.1, 240, 288, 258.5, 286.4, 288.0, 288.0, 0.5189952252439277, 0.5032834557297072, 0.267100081741748], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 10, 10, 100.0, 410.29999999999995, 383, 441, 409.0, 438.7, 441.0, 441.0, 0.5120589891955554, 0.16401889497670133, 0.26953104997695737], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6", 10, 0, 0.0, 260.0, 241, 281, 261.5, 279.4, 281.0, 281.0, 0.5195344970906068, 0.5043137598711555, 0.266870259247714], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 10, 10, 100.0, 405.2, 387, 433, 403.5, 431.7, 433.0, 433.0, 0.5134788189987163, 0.1644736842105263, 0.26275673940949934], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1", 10, 0, 0.0, 452.8, 414, 522, 443.5, 519.5, 522.0, 522.0, 0.5141652527122217, 2.1661219728520744, 0.2154071224741632], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0", 10, 0, 0.0, 415.90000000000003, 389, 477, 405.5, 473.0, 477.0, 477.0, 0.514588586425153, 0.8336938133587197, 0.25829934904543816], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3", 10, 0, 0.0, 259.7, 242, 270, 262.5, 269.6, 270.0, 270.0, 0.5189952252439277, 0.5032834557297072, 0.2721683944882707], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2", 10, 0, 0.0, 252.8, 242, 262, 256.0, 261.9, 262.0, 262.0, 0.5189952252439277, 0.5022697931804028, 0.2660864191924434], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1", 10, 0, 0.0, 447.9, 408, 531, 439.0, 525.7, 531.0, 531.0, 0.31709791983764585, 1.3357749873160831, 0.17898691178335868], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2", 10, 0, 0.0, 267.8, 240, 323, 264.0, 321.8, 323.0, 323.0, 0.3193153878085385, 0.3090249505061149, 0.21017438611616693], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0", 10, 0, 0.0, 405.7, 380, 453, 405.0, 449.40000000000003, 453.0, 453.0, 0.3175813008130081, 0.5145189238757623, 0.17956989567454268], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5", 10, 0, 0.0, 323.79999999999995, 239, 776, 266.5, 730.7000000000002, 776.0, 776.0, 0.31932558436581937, 0.30997034263635204, 0.21361135282283816], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6", 10, 0, 0.0, 268.09999999999997, 239, 323, 264.0, 321.8, 323.0, 323.0, 0.3193153878085385, 0.3099604448063352, 0.21048621754957372], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3", 10, 0, 0.0, 317.2, 239, 847, 256.0, 794.6000000000001, 847.0, 847.0, 0.3139421718519449, 0.30443806313377075, 0.2103167284086271], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4", 10, 0, 0.0, 269.5, 241, 323, 264.5, 321.8, 323.0, 323.0, 0.3193153878085385, 0.3096486133729284, 0.2107980489829805], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-23", 10, 10, 100.0, 404.0, 383, 433, 402.0, 432.3, 433.0, 433.0, 0.5150126178091363, 0.16496497914198896, 0.27510927923984135], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-23&currentTime=11:51", 10, 10, 100.0, 414.1, 386, 503, 401.5, 497.70000000000005, 503.0, 503.0, 0.5102561485865904, 0.16344142259414227, 0.2984799150423513], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["401/Unauthorized", 70, 100.0, 16.666666666666668], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 420, 70, "401/Unauthorized", 70, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-23", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-23&currentTime=11:51", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
